# Area6510

### What you can find here:

* /doc  
This directory contains some manuals and howtos.
* /releases  
Stable and/or development versions of various programs and their source code (if available).
    * /d64 or /d81  
    Stable or development releases of the program.
    * /src  
    The source code of the program (if available).
