# Area6510

### geoHDscsi
This is a small utility to change the current SCSI-device connected to your CMD-HD without the need to enable the configuration mode.

With version 1.02a there is also geoTDscsi, a special version for the RearAdmiral ThunderDrive HD.
