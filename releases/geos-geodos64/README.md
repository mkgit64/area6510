# Area6510

### GEODOS64
GeoDOS64 is a tool for the GEOS Graphical Desktop Environment:

* GeoDOS64 can handle DOS formatted 3,5" diskettes. It is possible to read files from a DOS-disk or write sequantial data files to a DOS-disk.
* GeoDOS64 also can convert files from/to geoWrite or DOS/Linux using converting tables. It can also re-format geoWrite documents using a default setup of font, printer or ruler settings.
* GeoDOS64 can also be used as a desktop replacement since it can delete/copy/rename files or whole disk.
* GeoDOS64 includes a new tool named GeoHelpView which can be used to display some kind of 'Online help' documentation.
