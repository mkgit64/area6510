# Area6510

### geoCham64RTC
This is a small utility to set the GEOS clock using the RTC of the Chameleon64.
Since version 1.3 it also supports the Ultimate64/II(+).

This application can be used as an AUTO_EXEC file or as an APPLICATION. Simply copy the file to your boot disk and it will be executed when GEOS is started. You can also run this file as an application from your desktop.
If Chameleon64 is not detected, the file will either be terminated without a message when GEOS starts, or it will display a short error message when started as an application.
