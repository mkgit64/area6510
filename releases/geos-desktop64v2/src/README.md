# Area6510

### DESKTOP64
This is the source code for the original GEOS/DESKTOP64 V2.x.

#### SOURCE CODE V2.0
The archived version 2.0 of the source code compiles an exact 1:1 copy of german version "DESK TOP" using GEOS/MegaAssembler.
An english version can be build but does not match the original english version of "DESK TOP" since it was translated using the german code base.

#### SOURCE CODE V2.1
Version 2.1 and beyond includes some small fixes and code cleanups but no major improvements.
