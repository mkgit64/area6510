# Area6510

### DESKTOP64
This is the reassembled GEOS/DESKTOP64 V2.

Version 2.1 and beyond includes some small fixes and code cleanups but no major improvements:

* System files are no longer removed from 'invalid' boot disks.
* GEOS serial number no longer written to program disks.
* Very long printer names displayed correctly next to the printer icon.
* Bug in screen buffer routine for dialogue boxes fixed.
* Options/Exit calls the EnterDeskTop routine.
  (Return to the installed DeskTop interface).
* Unused code removed.
* Unnecessary commands due to macros removed.
