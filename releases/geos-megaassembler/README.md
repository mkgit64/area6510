# Area 6510

### GEOS/MegaAssembler V5
The original MegaAssembler V2.0 is fine for average sized projects, but if you are working on very large projects there are some things that could have been done better:

##### Selecting source files
MegaAssembler's menu for selecting source files only shows the first 13 entries on the disc. If you have more source files, you have to find a way around this.
Version 3.x and later only shows the first 12 entries, but includes an extra entry to show the next 12 entries.

##### Compiling many source files
By default, the MegaAssembler only compiles one source file at a time.
Version 3.x introduces the AutoAssembler mode, where you create a configuration file containing the sources to be compiled. When the AutoAss option is enabled, you can select the configuration file and MegaAssembler will then compile all the source files without having to manually select the source files from the menu.

##### Symbol Table
The free space for the symbol table was only about 16Kb. Version 3.x has been reworked to save some memory and the symbol table now provides about 24Kb of memory.

##### Miscellaneous
Some new pseudo opcodes have been added such as 'h' to add text to the info block of GEOS files ok 'k'/'l' to add the current date in short/long format to the source code as a text string. The available pseudo opcodes can be viewed in the 'Exit' menu.

#### HOW TO INSTALL
You must build the application yourself using an existing copy of GEOS/MegaAssembler V2. Order of assembly:

* src.MegaAss0
* src.MegaAss1
* src.MegaAss2
* src.MegaAss3
* src.MegaAss4
* src.MegaAss5
* src.MegaAss6

Once all the parts are assembled, open VLink to link the final application:

* lnk.MegaAss

You should now have a new application file on your disk called "MegaAss Vx.y".