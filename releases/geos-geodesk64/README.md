# Area6510

### GEODESK64
GeoDesk is a new DeskTop for GEOS/MegaPatch64, GEOS/MegaPatch128 is not supported.

GeoDesk requires at least 256Kb of RAM/GEOS-DACC and supports drag'n'drop, multiple windows and supports all CMD hardware and SD2IEC devices.
Note: All colors can be turned off if you prefere using the old "grey" color style from GEOS 2.x!

![screenshot#1](geodesk64-01.png "GeoDesk64 Screenshot #1")
![screenshot#2](geodesk64-02.png "GeoDesk64 Screenshot #2")
![screenshot#3](geodesk64-03.png "GeoDesk64 Screenshot #3")
![screenshot#4](geodesk64-04.png "GeoDesk64 Screenshot #4")
