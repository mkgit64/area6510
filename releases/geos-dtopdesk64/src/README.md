# Area6510

### DTOPDESK64
This is the reconstructed and updated source code from TopDesk64 V5.0de R.01 (2021/12/28).

This directory contains the following subdirectories:
* src-topdesk64-rebuild: This is the cleaned up version of the source code.
* src-dtopdesk: The current development branch of DTopDesk64 which uses the optimised and bug-fixed source code.
